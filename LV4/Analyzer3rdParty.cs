﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int row = data.Count();
            int column = data[0].Count();
            double[] result = new double[column];
            for(int i = 0;i<column;i++)
            {
                for(int j = 0; j < row;j++)
                {
                    result[i] += data[j][i];
                }
                result[i] /= data.Length;
            }
            return result;

        }

    }
}
