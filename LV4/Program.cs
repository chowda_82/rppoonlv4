﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("test.txt");
            Analyzer3rdParty analize = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analize);
            Console.WriteLine(adapter.CalculateAveragePerColumn(data));

            double[] result = adapter.CalculateAveragePerColumn(data);

            foreach (double i in result)
            {
                Console.WriteLine(i);
            }

            List<IRentable> list = new List<IRentable>();
            Video video = new Video("Godfather");
            Book book = new Book("Shawshank redemption");
            list.Add(video); list.Add(book);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(list);
            printer.PrintTotalPrice(list);
        }
    }
}
